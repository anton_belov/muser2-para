/*----------------------------------------------------------------------------*\
 * File:        toolcfg.hh
 *
 * Description: Defines used by the muser tool.
 *
 * Author:      jpms
 * 
 *                                     Copyright (c) 2009, Joao Marques-Silva
\*----------------------------------------------------------------------------*/

#ifndef _TOOLCFG_H
#define _TOOLCFG_H 1

namespace {

  const char* build_date = BUILDDATE;
  const char* toolname = "MUSer2-PARA";
  const char* output_file = "muser2-output"; // prefix only
  const char* authorname = "Anton Belov (MUSer2), Norbert Manthey (parallelization), Joao Marques-Silva (MUSer)";
  const char* authoremail = "";
  const char* contribs = "";
  const char* commit_id = COMMITID;

}

#endif /* _TOOLCFG_H */

/*----------------------------------------------------------------------------*/
