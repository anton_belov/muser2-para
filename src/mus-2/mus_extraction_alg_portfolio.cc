/**----------------------------------------------------------------------------*\
 * File:        mus_extraction_alg_del.cc
 *
 * Description: Implementation of the portfolio MUS exctraction algorithm.
 *
 * Author:      antonb, norbert
 * 
 * Notes:
 *
 *                          Copyright (c) 2011-12, Anton Belov, Norbert Manthey
\*----------------------------------------------------------------------------*/

#include <cstdio>
#include <iostream>
#include <sstream>
#include "basic_group_set.hh"
#include "mus_extraction_alg.hh"

/** Norbert: the tool needs to know about schedulers! */
#include "random_scheduler.hh"
#include "parsing.hh"

namespace {
}

/** The main extraction logic is implemented here. As usual the method does 
 * not modify the group set, but rather computes the group ids of MUS groups
 * in MUSData
 */

/** Norbert */

/// for synchronization
struct syncData {
  pthread_mutex_t mutex;
  pthread_cond_t master_cv;
};

/// Main structure for parallel data
struct workData {
  ToolConfig* config;
  IDManager* imgr;
  BasicGroupSet* gset;
  MUSData* md;
  SATChecker* schecker;
  GroupScheduler* sched;
  ModelRotator* mrotter;
  MUSExtractionAlg* pmus_alg;
  syncData* syncdata;
  bool finished;
};


static void* run (void* data)
{
  
  pthread_attr_t attr;
  pthread_attr_init (&attr);
  pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_JOINABLE);
  
  workData* wd = (workData*)data;
  
  MUSExtractionAlg& mus_alg(*(wd->pmus_alg));
  mus_alg();
  
  // important: first set the value to true, afterwards send the signal!
  wd->finished = true;
  // tell master thread that we are done (in critical section to make sure the master sleeps already)
  pthread_mutex_lock (&(wd->syncdata->mutex));
  pthread_cond_signal (&(wd->syncdata->master_cv));
  pthread_mutex_unlock (&(wd->syncdata->mutex));
  // finally, the thread exits
  return 0;
}

void MUSExtractionAlgPortfolio::operator()(void)
{
  
  ce << endl;
  
  const int threads= config.get_threads();
  // copy everything here twice in extra data structures 
  // in the order the things are created:

  ToolConfig* configs[threads];           // configuration (name is good for macros)
  IDManager* imgrs[threads];             // id manager
  
  BasicGroupSet* gsets[threads];         // set that stores the clauses
  MUSData* mds[threads];                 // MUS data to work on
  
  SATChecker* scheckers[threads];        // the SAT checker
  GroupScheduler* scheds[threads];       // group scheduler 
  ModelRotator* mrotters[threads];       // the model rotator
  MUSExtractionAlg* algos[threads];      // the selected algorithms

  workData threadData[threads];		  // data that will be passed to the threads
  
  pthread_t threadIDs[threads];
  syncData* syncdata = new syncData;
  pthread_mutex_init(&(syncdata->mutex), NULL);
  pthread_cond_init (&(syncdata->master_cv), NULL);
  
  ce << "created pointer arrays" << endl;
  
  for( int i = 0 ; i < threads; ++i )
  {
    ce << "created data for thread " << i << endl;
    
    configs[i] = new ToolConfig( config ); // FIXME: unsafe, there are char-arrays inside the object, but only the pointer is copied    
    imgrs[i] = new IDManager( _imgr ); // usual copy constructor

    // create new set by loading the file again
    gsets[i] = new BasicGroupSet(*(configs[i]));
    load_file(configs[i]->filename, (*configs[i]), (*imgrs[i]), (*gsets[i]));

    mds[i] = new MUSData(*(gsets[i]));
    mrotters[i] = new RecursiveModelRotator();   // nothing special here!

    // select SAT solvers here
    scheckers[i] = new SATChecker(*(imgrs[i]), *(configs[i]), i); // separate IDs are good for tracing
    
    // select the scheduler
    scheds[i] = new RandomScheduler( *(mds[i])); // since we will have a portfolio, each thread will have a random scheduler (first 4 scheduler could be the deterministic ones)

    // select the extraction algorithm
    algos[i] = 0;
    int choice = 0; // rand() % 3;
    if (choice == 0)
      algos[i] = new MUSExtractionAlgDel(*(imgrs[i]), *(configs[i]), *(scheckers[i]), *(mrotters[i]), *(mds[i]), *(scheds[i]));
    else if (choice == 1) 
      algos[i] = new MUSExtractionAlgIns(*(imgrs[i]), *(configs[i]), *(scheckers[i]), *(mrotters[i]), *(mds[i]), *(scheds[i]));
    else if (choice == 2) 
      algos[i] = new MUSExtractionAlgDich(*(imgrs[i]), *(configs[i]), *(scheckers[i]), *(mrotters[i]), *(mds[i]), *(scheds[i]));
    
    // put all pointers into the structure!
    threadData[i].config = configs[i];
    threadData[i].gset = gsets[i];
    threadData[i].imgr = imgrs[i];
    threadData[i].md = mds[i];
    threadData[i].mrotter = mrotters[i];
    threadData[i].schecker = scheckers[i];
    threadData[i].sched = scheds[i];
    threadData[i].pmus_alg = algos[i];
    threadData[i].finished = false;
    threadData[i].syncdata = syncdata;		// to be able to send the signal for the conditional variable!
  }
  
  // create threads (in locked section so that they cannot call release the master before it is asleep
  pthread_mutex_lock (&(syncdata->mutex));
  
  for( int i = 0 ; i < threads; ++ i )
  {
    pthread_attr_t attr;
    pthread_attr_init (&attr);
    pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_JOINABLE);
  // create one thread
    int rc = pthread_create( &(threadIDs[i]), &attr, run, (void *)  &(threadData[i]));
    if (rc) {
      fprintf(stderr,"ERROR; return code from pthread_create() is %d for thread %d\n",rc,i);
    }
    pthread_attr_destroy(&attr);
    
  }
  // wait for the first to finish
  
  pthread_cond_wait (&(syncdata->master_cv),&(syncdata->mutex));
  
  pthread_mutex_unlock (&(syncdata->mutex));
  
  // copy solution back
  
  // select finishing thread!
  
  // run all the algorithms sequentially!
  int i = 0;
  for( ; i < threads; ++ i )
  {
    if( threadData[i].finished == true ) break;
  }
  assert( i != threads && "there has to be one finished thread!" );
  if( i == threads ) {
    ce << "execution error, abort" << endl;
    exit(-1);
  }
  ce << " finished with ["<< i << "] calls: " << threadData[i].pmus_alg->sat_calls() << " rot groups: " << threadData[i].pmus_alg->rot_groups() << " ref_groups: " << threadData[i].pmus_alg->ref_groups() << endl;
  _md.copy_from(*threadData[i].md);

  // not very clean, but fast:
  if (fork()) return;
  
  // do not kill anybody
  
  for( int i = 0 ; i < threads; ++ i )
  {
    pthread_kill(threadIDs[i],15);
  }
  
  // after killing everybody, exit!
  exit(0);
}

// local implementations ....

namespace {

}

/**----------------------------------------------------------------------------*/


