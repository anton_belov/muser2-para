/**----------------------------------------------------------------------------*\
 * File:        mus_extraction_alg_tmp.cc
 *
 * Description: Implementation of the deletion-based MUS exctraction algorithm.
 *
 * Author:      antonb, norbert
 * 
 * Notes:
 *
 *                          Copyright (c) 2011-12, Anton Belov, Norbert Manthey
\*----------------------------------------------------------------------------*/

#include <cstdio>
#include <iostream>
#include <sstream>
#include "basic_group_set.hh"
#include "mus_extraction_alg_tmp.hh"
#include "mt_utils.hh" // co_mt, cop_mt, ce_mt, cep_mt
#include "macros.hh"

/** Norbert: lock structures */
#include "parallel.hh"
#include "Communication.h"

// compilation with 'make mkm=chk' will enable define CHK(x) to be x globally --
// this is designed for the checked mode, i.e. for invariant tests that are more 
// expensive than the usual assertions; uncommenting the line below enables these
// checks only for this file
//#define CHK(x) x

// same thing for debug mode: 'make mkm=dbg' and DBG(x)
// #define DBG(x) x

// finally: to enable assertions, compile with 'make mkm=ass'

namespace {

  /** Reposible for updating the state of the worker (worker_md) to match
   * the state of the master (master_md). 
   * Invariant: master_md is a "subset" of worker_md (in terms of group set)
   */
  void update_state(MUSData& master_md, MUSData& worker_md);

  // ANTON: I think these are all obsolete now ...

  /** send interrupt signal to all threads!
   *  should be called by the master only!
   */
  void sendInterruptToAllThreads(ThreadInfo** info, int threads);

  /** Wait until all threads are either finished or "not-working" (in communication)
   *  should be called by the master only!
   *  Note: it is assumed, that master already grabed its lock!!
   */
  void waitForAllThreadsBeingInterrupted(ThreadInfo** info, SleepLock* masterLock, int threads);

  /** intterupt all threads based on communication (within the solver)
   *  should be called by the master only!
   *  Note: returns, if all solver have either left search already, or until they are interrupted inside their search loop
   *  Note: it is assumed, that master already grabed its lock!!
   */
  void interruptAllThreads( ThreadInfo** info, SleepLock* masterLock, int threads );

  /** let all solvers (except finished ones) receive the clauses!
   * assumption 1: masterlock is grabed
   * assumptino 2: each solver is waiting (either finished, or within the sleep loop! (updateSleep-method))
   */
  void letSolversReceiveMasterUnits(ThreadInfo** info, SleepLock* masterLock, int threads);

}

/** class that stores the necessary methods for a solving thread
 */
class ThreadInfo {

public:
  // attributes
  SleepLock* masterLock = nullptr;
  SleepLock* ownLock = new SleepLock();

  // TODO: think about read and write. master writes, client polls, could set back to poll again
  enum State{
    created = 0,     // initial state, can't do anything useful yet
    idle = 1,        // ready, but has no work at the moment
    working = 2,     // is simply working
    interrupted = 3, // interrupt current run, proceed with the run afterwards!
    aborted = 4,     // abort current solving run and wait for next work item
    doExit = 5,      // at next interruption thread is canceled
    sleeping = 6,    // the thread currently sleeps
    finished = 7,    // thread finished its work on the current group
    waiting = 8,     // master proecsses data of the thread, thread should be waiting
  };

  MUSData* md = nullptr;        // this thread's own copy of MUSData (but remember
                                // that group-set is shared !
  CheckGroupStatus* workItem = nullptr;
  SATChecker* _schecker = nullptr;
  IDManager* _imgr = nullptr;
  Communicator* communicator = nullptr;
  int id = -1;       // id of this thread
  int verbosity = 0; // verbosity for this thread
  bool use_stamps = false; // whether or not to do stamping (to block back comm)

private:

  State state = created;    // no public access any more, use methods!

  char dummy[64]; // to separate this data on extra cache lines (avoids false sharing)

public:
  // methods

  // seet default values, ownLock is set to initial sleep
  ThreadInfo(void) {}

  // destructor
  ~ThreadInfo(void)
  {
      if( ownLock != 0 ) delete ownLock;
  }

  bool isIdle() const { return state == idle; }
  bool isWorking() const { return state == working; }
  bool isFinished() const { return state == finished; }
  bool isAborted() const { return state == aborted; }
  bool isWaiting() const { return state == waiting; }

  /** change the state of the worker, ensures valid transitions
   *  note: lock ownlock before!
   *        should be called by the master
   * @return false, if invalid transition was tried
   */
  bool setStateM( const State s )
  {
    assert( s != finished && "master is not allowed to set state to finished" );
    assert( (state != working  || s != idle ) && "master is not allowed to set state from working to idle" );
    state = s;
    if ((s == working) && communicator != nullptr) {
//       cerr << "c change communicator from " << communicator->getState() << " to working ?!" << endl;
//       assert( (communicator->isWorking() || communicator->isAborted() )&& "Communicator should be working always!" );
      communicator->setState(Communicator::working);
    } else if ((s == waiting) && communicator != nullptr) {
      communicator->setState(Communicator::waiting);
    }
    return true;
  }

  /** change the state of the worker, ensures valid transitions
   *  note: lock ownlock before!
   *        should be called by the worker
   * @return false, if invalid transition was tried
   */
  bool setStateW( const State s )
  {
    assert( s != working && "worker is not allowed to set its state to working" );
    assert( (state != idle || s != finished) && "worker is not allowed to change state from idle to finished" );
    // should we set the communicator state here?
    state = s;
    return true;
  }

  /** return state for the outside
   */
  const State getState() const { return state; };

};


/**
 * Main method that is executed by a worker thread. The method assumes that
 * all the relevant data (passes as a parameter) has already been initialized.
 */
static void* run (void* data)
{
  // tell thread that it can be interrupted
  pthread_attr_t attr;
  pthread_attr_init (&attr);
  pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_JOINABLE);

  ThreadInfo& info = * ( (ThreadInfo*)data );
  int verbosity = info.verbosity;
  CheckGroupStatus& wi = *(info.workItem);
  SATChecker& schecker = *(info._schecker);
  bool use_stamps = info.use_stamps;
  
  if( verbosity > 0 )
    cep_mt << "[THREAD " << info.id <<"] started with SATchecker at "
            << hex << &schecker << " and workItem at " << &wi << dec << endl;
	    
  DBG( cep_mt << "c create thread " << gettid() << " as thread with ID " << info.id << endl; );

  // first off all, load the groupset into the solver, this has to be done
  // by all threads before there are any changes to the groupset itself (
  // remember that the actual groupset is shared !)
  schecker.load_groupset(*info.md);

  // now, get into the idle state, wake up master, and wait until we have
  // some work
  info.ownLock->lock(1010);
  info.setStateW( ThreadInfo::idle );
  if (verbosity > 1)
    cep_mt <<  "[THREAD " << info.id << "] loaded group set, ready to roll ..." << endl;
  info.masterLock->awake(1020);
  if (!info.isWorking()) { info.ownLock->sleep(1030); }
  info.ownLock->unlock(1040);

  // proceed with the current work item (group) as long as required
  while( !info.isAborted() )
  {
    // setup work
    if( verbosity > 1 )
      cep_mt <<  "[THREAD " << info.id << "] processing group " << wi.gid() << endl;
    
    // do work, first ensure that the environment is set to the right values again
    // info.communicator->setState( Communicator::working );
    // assert( info.communicator->isWorking() && "thread has to be set to working by master (or aborted)" );
    bool isAbortBefore = info.communicator->isAborted();
    if( ! info.communicator->isWorking() ) cerr << "c thread " << info.id 
      << " is not working, but " << info.communicator->getState() << endl;
    
    // update the stamp -- its the version of MUSData
    if (use_stamps) { info.communicator->set_stamp(wi.md().version()); }

    // todo: if state != working, do not process wi (saves CPU time)
    schecker.process(wi);
    
    if( isAbortBefore && ! info.communicator->isAborted() ) {
      cep_mt << endl << endl
             << "state of thread " << info.id << " has been changed during SAT solver call from aborted (" << Communicator::aborted << ") to " << info.communicator->getState() << endl;
    }
    
    // if( !info.communicator->isWorking() )
    //  cep_mt <<  "[THREAD " << info.id << "] finished group " << wi.gid() <<  " with state " << info.communicator->getState() << endl;
    
    if( verbosity > 1 )
      cep_mt <<  "[THREAD " << info.id << "] finished group " << wi.gid() << endl;
    
    // set own state to finished (write -> lock)
    info.ownLock->lock(1050);			// TODO: necessary to have such a big lock section?
    
    if( verbosity > 2 ) cep_mt <<  "[THREAD " << info.id << "] change state to finished" << endl;
    info.setStateW( ThreadInfo::finished );
    
    if( verbosity > 2 )
      cep_mt <<  "[THREAD " << info.id << "] wake up master" << endl;
    // wake up master so that the master can do something with the solver
    assert(info.isFinished() && "thread that awakes master thread should be finished!" );
    info.masterLock->awake(1060);
    
    if( verbosity > 2 )
      cep_mt <<  "[THREAD " << info.id << "] wait for next item (sleep)" << endl;

    // wait until master changes the state again! (need to lock here, because of sleep)
    if( ! info.isWorking() )
      info.ownLock->sleep(1070);
    else {
      cep_mt <<  "[THREAD " << info.id << "] wanted to wait, but already put to work." << endl;
    }
    if( !info.isWorking() ) {
      cep_mt << "c [FAIL] thread[" << info.id << "] with state " << info.getState()
             << " and gid " << wi.gid() << " has been woken up" << endl;
      assert(false && "when thread is waked up, it should be working" ); 
    }
    info.ownLock->unlock(1080);
  }
  if( verbosity > 0 )
    cep_mt <<  "[THREAD " << info.id << "] aborted." << endl;
  return 0;
}


/**
 * This is an experimental version of the parallel MUS extraction algorithm.
 * This method contains the logic of the master thread.
 */
void MUSExtractionAlgTmp::operator()(void)
{
  // initialize parameters and data
  masterVerbosity = config.get_verbosity();
  threads = config.get_threads();
  useSameGID = config.get_same_group();
  waitForAll = config.get_wait_for_all();
  useOldCores = config.get_use_old_cores();
  cbs = config.get_cbs();
  _r_gids.clear(); _r_vers.clear(); _r_vers.push_back(0);
  info = new ThreadInfo*[threads];
  masterLock = new SleepLock();
  _isFinished.resize(threads);
  communicationData = new CommunicationData(); // this could also be made static
  threadIDs = new pthread_t[threads];          // thread ids
  
  if (masterVerbosity > 0)
    cep_mt << "[MASTER, " << RUSAGE::read_cpu_time_thread() << " sec] initializing with "
           << threads << " threads, " << "same_group=" << useSameGID
           << ", wait_for_all=" << waitForAll << ", communication=" << config.get_comm() << endl;
  
  initWorkerThreads();
  if (startWorkerThreads()) {
    tool_abort("unable to start worker threads");
  }
  
  if (masterVerbosity > 0)
    cep_mt <<  "[MASTER, " << RUSAGE::read_cpu_time_thread()
           << " sec] init done, start search" << endl;

  //
  // main loop: iterate over all groups and check their necessity
  //
  for (int iteration = 1; ; ++iteration)
  {
    if (masterVerbosity > 1)
      cep_mt <<  "[ITERATION " << iteration << "] start" << endl;
    
    if (hasUnscheduledGroups()) {
      // kick off workers (this will lock masterLock)
      if (masterVerbosity > 1)
	printThreadStates(cep_mt);
      startIdleThreads();
    } else {
      // with waitForAll=false it might be that all groups are scheduled, but
      // there are threads that have not finished, we will wait for them
      if (allThreadsAreWaiting()) { // finished !
        break;
      }
      if (masterVerbosity > 1)
        cep_mt << "[MASTER] all groups are done, but some threads are still running"
               << endl;
      masterLock->lock(1090);
    }
    // at least one thread has to be running, or it is finished!
    
    // wait for someone or for all
    if (waitForAll) {
      waitForAllThreadsToFinish();
    } else {
      int finishedThread = waitForSomeThreadToFinish();
      if (finishedThread == threads) {
        // TODO: figure out how to handle the error properly --
        if (masterVerbosity > 1)
	  printThreadStates(cep_mt);
        assert( false && "after the master has been waked up there has to be a result!" );
      }
    }
    if (masterVerbosity > 2)
        cep_mt << "[MASTER] at least one thread finished since last time"
               << endl;
    masterLock->unlock(1200);

    // some threads are done -- we're going to collect and merge the results
    // of all threads that are in finished state at this particular moment
    collectResults();
    mergeResults();

    // ok, by now we might have some new threads that have finished while we were
    // merging, and also some threads that are still running - we treat them all
    // in the same way: threads that are/were working on groups whose status
    // became known are aborted, and, once they are finished, all finished threads
    // are put into waiting state and their md's are synchronized to the global _md
    abortUselessThreads();
    prepareFinishedThreads();

    /* FOR ANTON
    /// with this method, the master can add unit literals to the globally shared data field 
    vector<Minisat::Lit> unitLiterals;
    // first parameter is the ID of the thread. Since 0-(thread-1) are already used
    // by the working threads, the master simply picks the next number
    communicationData->getBuffer().addUnitClauses(threads,unitLiterals);
    */
    
    if (masterVerbosity > 1)
      cep_mt <<  "[ITERATION " << iteration << "] end" << endl;

  } // main loop


  if (masterVerbosity > 0)
    cep_mt <<  "[MASTER] all groups are done and all threads are finished." << endl;

  computeAndDumpStats();

  // TODO: destroy everything and free up the memory allocated for threads ...
  // or not ;-)

}

//// implementation of all the private methods ...

/** Initializes the ThreadInfo structures of the worker threads
 */
void MUSExtractionAlgTmp::initWorkerThreads(void)
{
  for( int i = 0 ; i < threads; ++ i )
  {
    // ANTON: SAT solvers use the ID-manager to create activation variables.
    // Each should have its own manager, to avoid races -- later we could make
    // an MT-safe ID manager (with C++11 atomic<>)
    // Norbert: TODO: it would be better, if all SAT solvers share the activation
    // variables. Activating clause 1 in solver1 should be the same variable as
    // activating clause 1 in solver2 (important for sharing)
    // ANTON: this is what happens at the moment, since all SAT solvers get a
    // copy of the same manager, and process clauses in the same order. But, its
    // brittle. TODO: figure out a good way to do this (store lit with clause ?)
    info[i] = new ThreadInfo();
    info[i]->masterLock = masterLock; // set link to master lock, ownLock is already used
    info[i]->md = new MUSData(_md.gset());  // new md, but old group set
    info[i]->_imgr = new IDManager(_imgr);  // store needed only to free up later
    info[i]->_schecker = new SATChecker(*info[i]->_imgr, config, i);
    assert( info[i]->_schecker != 0 && "failed to create another SAT checker" );
    info[i]->verbosity = config.get_verbosity();
    info[i]->communicator = new Communicator(i, communicationData);
    info[i]->communicator->setState( Communicator::working );
    info[i]->use_stamps = config.get_block_backcomm();
    // communication parameters
    info[i]->communicator->setDoSend(config.get_comm());
    info[i]->communicator->setDoReceive(config.get_comm());
    info[i]->communicator->protectAssumptions = config.get_protect_assumptions();
    info[i]->communicator->sendSize = config.get_send_size();
    info[i]->communicator->sendLbd = config.get_send_lbd();
    info[i]->communicator->sendMaxSize = config.get_send_max_size();
    info[i]->communicator->sendMaxLbd = config.get_send_max_lbd();
    info[i]->communicator->sizeChange = config.get_size_change();
    info[i]->communicator->lbdChange = config.get_lbd_change();
    info[i]->communicator->sendRatio = config.get_send_ratio();
    info[i]->communicator->doBumpClauseActivity = config.get_do_bump_clause_activity();
    info[i]->communicator->receiveEvery = config.get_reject_receive_every();

    if (masterVerbosity > 2)
      cep_mt << "[MASTER] created communicator for thread " << i << " at "
        << std::hex << info[i]->communicator
        << " ,where schecker=" << info[i]->_schecker
        << " and schecker->solver=" << &(info[i]->_schecker->solver()) << std::dec << endl;

    info[i]->_schecker->solver().setCommunicator( info[i]->communicator );


    if( masterVerbosity > 2 )
      cep_mt <<  "[MASTER] setup thread " << i << " with data at " << std::hex
         << info[i]  << " with SAT checker at " << info[i]->_schecker << std::dec << endl;

    // setup and initialize a work item for each solver
    info[i]->workItem = new CheckGroupStatus(*info[i]->md, gid_Undef); // pass a reference to MUS data
    info[i]->workItem->set_refine(config.get_refine_clset_mode());  // refine clset if applicable
    info[i]->workItem->set_need_model(config.get_model_rotate_mode());
    info[i]->workItem->set_use_rr(config.get_rm_red_mode() || config.get_rm_reda_mode());
    info[i]->workItem->set_save_core(config.get_cbs());
    info[i]->id = i;
  }
}

/** Releases all the memory allcated in initWorkerThreads
 */
void MUSExtractionAlgTmp::deinitWorkerThreads(void)
{
  // TODO
}

/** Starts the worker threads, and waits until they get into the idle state.
 * The IDs of created threads are recorded in threadIDs.
 * @pre masterLock is not grabbed
 * @return 0 on success, UNIX errno in case of failure
 */
/** Terminates all the worker threads.
 */
void MUSExtractionAlgTmp::stopWorkerThreads(void)
{
  // TODO
}

/** Starts all currently idle (or waiting) threads. The first thread is started
 * to work on the group _nextUnscheduledGroup; the rest - depending on useSameGID
 * parameter.
 * Notes:
 *   - should be called by the master only
 *   - assumes that masterLock is not owned at the time of the call
 *   - masterLock will be owned on return
 */
void MUSExtractionAlgTmp::startIdleThreads(void)
{
  // repeat until all threads are in work (and some of them have results)
  GID last_group = gid_Undef;
  while( true ) {
    // evaluate the state of all the threads
    masterLock->lock(1240);
    if( masterVerbosity > 2 ) cep_mt <<  "[MASTER] evaluate ..." << endl;
    // check whether there are free threads
    int nextThread = threads;
    for( int i = 0 ; i < threads; ++ i ) {
      // client never changes state from idle to something (should be enforced
      // at some point) thus, reading should be safe!
      if( info[i]->isIdle() || info[i]->isWaiting() ) { nextThread = i; break; }
    }
    // if no free threads - we're done (note: still has the master lock !)
    if (nextThread == threads) {
      if( masterVerbosity > 2 ) {
        cep_mt << "c [MASTER] found no idle threads" << endl;
        printThreadStates(cep_mt);
      }
      return;
    }
    // since thread is idle, it should not be running, but waiting in the sleep lock
    masterLock->unlock(1250); // leave critical section
    assert( (info[nextThread]->isIdle() || info[nextThread]->isWaiting())
            && "the next thread needs to be idle" );

    // setup threads work data
    CheckGroupStatus& wi = *(info[nextThread]->workItem);
    wi.reset();
    // pick the group (unless its the first time)
    if (last_group != gid_Undef && !useSameGID)
      if (!hasUnscheduledGroups()) // ran out: ok schedule this thread for same group
        _nextUnscheduledGroup = last_group;
    wi.set_gid(last_group = _nextUnscheduledGroup);
    markScheduled(last_group);
    if( masterVerbosity > 1 )
      cep_mt <<  "[MASTER] assign group " << wi.gid() << " to thread " << nextThread << endl;
    info[nextThread]->ownLock->lock(1260);
    if( masterVerbosity > 1 )
      cep_mt <<  "[MASTER] set thread " << nextThread << " state to working" << endl;
    info[nextThread]->setStateM(ThreadInfo::working);
    info[nextThread]->ownLock->unlock(1270);
    if( masterVerbosity > 1 )
      cep_mt <<  "[MASTER] awake thread " << nextThread << endl;
    info[nextThread]->ownLock->awake(1280);
  } // while (true) ...
}


/** Waits for *some* thread to finish, i.e. to get into the finished state.
 * Returns the index of the thread that finished, or threads in case of error.
 * Note:
 *   - should be called by the master only
 *   - assumes that masterLock is owned
 */
int MUSExtractionAlgTmp::waitForSomeThreadToFinish(void)
{
  int finishedThread = threads;

  // fast check (its actually needed !)
  for( int i = 0; i < threads; ++i )
    if( info[i]->isFinished() ) { finishedThread = i; break; }

  if (finishedThread == threads) { // nope, need to wait
    if (masterVerbosity > 2) {
      cep_mt <<  "[MASTER] waiting for some thread to finish" << endl;
      printThreadStates(cep_mt);
    }
    // if some thread has a result yet, it has to wake up the master
    // (since state checking is in a critical section, here cannot be a deadlock)
    masterLock->sleep(1290);
    if( masterVerbosity > 2 ) {
      cep_mt <<  "[MASTER] woke up " << endl;
      printThreadStates(cep_mt);
    }
    // at this position there has to be someone that's finished, b/c how else
    // could the master be awaken
    for( int i = 0 ; i < threads; ++ i )
      if( info[i]->isFinished() ) { finishedThread = i; break; }
  }
  assert(finishedThread < threads
         && "after the master has been waken up someone had to finish!" );
  if (masterVerbosity > 2) {
    cep_mt << "[MASTER] found finished thread " << finishedThread << endl;
    printThreadStates(cep_mt);
  }
  return finishedThread;
}


/** Waits for *all* threads to finish, i.e. to get into the finished state.
 * Returns the index of the thread that finished, or -1 in case of error.
 * Note:
 *   - should be called by the master only
 *   - assumes that masterLock is owned
 */
void MUSExtractionAlgTmp::waitForAllThreadsToFinish(void)
{
  if (masterVerbosity > 2)
    cep_mt <<  "[MASTER] waiting for all threads to finish" << endl;
  for (int i = 0; i < threads; i++) {
    while (!info[i]->isFinished())
      masterLock->sleep(1300);
    if (masterVerbosity > 2)
      cep_mt <<  "[MASTER] thread " << i << " is done." << endl;
  }
  if (masterVerbosity > 2) {
    cep_mt << "[MASTER] all threads are finished" << endl;
    printThreadStates(cep_mt);
  }
}


// @return true, if thread has been finished before check
//
bool MUSExtractionAlgTmp::endSolverNicely(int thread)
{
  masterLock->lock(1310); // TODO: lock is not necessary, because the lock has been taken outside the method already?!
  
  bool isAlreadyFinished = signalNiceEnd(thread);
  if( isAlreadyFinished ) {
    masterLock->unlock(1315); // TODO: don't!! (careful! - no sure)
    return true;
  }

  // wait until solver is finished! assumed not to be grabbed already
  while ( true )
  {
    if( info[thread]->isFinished() ) break; // finished, no need to wait any longer
    // TODO: stats about wrong awakes would be nice to get a feeling whether there is room for more improvement
    masterLock->sleep(1320);               // some other thread called awake -> wait again!
  }
  masterLock->unlock(1330); // TODO: don't!! (careful! - no sure)
  return false;
}

/** as method endSolverNicely tells solver to abort */
bool MUSExtractionAlgTmp::signalNiceEnd(int thread)
{
  // read only -> safe without lock
  bool isAlreadyFinished = info[thread]->isFinished();
  if( isAlreadyFinished ) return true;

  // if solver is still working, the solver is aborted (terminated nicely)
  // solver does not update state after communicator::aborted -> safe without lock
  if( info[thread]->isWorking() )
    info[thread]->communicator->setState( Communicator::aborted );
  return false;
}

/** return after all threads which are marked as aborted are also finished
 *  Note:
 *   - should be called by the master only
 *   - assumes that masterLock is grabbed already
 */
void MUSExtractionAlgTmp::waitForAllAbortedSolvers(void)
{
  bool check[threads];
  for( int i = 0 ; i < threads; ++i ) {
    check[i] = info[i]->communicator->isAborted();
    if (masterVerbosity > 2) {
      cep_mt << "[MASTER] wait for aborted thread " << i << " with current state " << info[i]->getState() << endl;
      printThreadStates(cep_mt);
    }
  }

  // check all threads with abort-flags
  for( int pos = 0; pos < threads; ++ pos )
  {
    if( !check[pos] ) continue;
    while( !info[pos]->isFinished() ) {
      masterLock->sleep(1340);
    }
  }
}

/** Returns true if all threads are in the waiting state. This is just a quick
 * check, without any locking */
bool MUSExtractionAlgTmp::allThreadsAreWaiting(void)
{
  for( int i = 0 ; i < threads; ++i )
    if (!info[i]->isWaiting()) { return false; }
  return true;
}


/** Dumps the current state of all threads */
void MUSExtractionAlgTmp::printThreadStates(ostream& out)
{
  ostringstream s;
  s << "c thread states:";
  for( int i = 0 ; i < threads; ++ i )
  {
    s << " |";
    switch( info[i]->getState() ){
    case ThreadInfo::idle: s << " idle"; break;
    case ThreadInfo::working: s << " working"; break;
    case ThreadInfo::interrupted: s << " interrupted"; break;
    case ThreadInfo::aborted: s << " aborted"; break;
    case ThreadInfo::doExit: s << " doExit"; break;
    case ThreadInfo::sleeping: s << " sleeping"; break;
    case ThreadInfo::finished: s << " finished"; break;
    case ThreadInfo::waiting: s << " waiting"; break;
    default: s << " " << info[i]->getState();
    }
    s << ",";
    switch( info[i]->communicator->getState() ){
      case Communicator::idle: s <<                "idle"; break;
      case Communicator::working: s <<             "working"; break;
      case Communicator::interrupted: s <<         "interrupted"; break;
      case Communicator::interruptedForce: s <<    "interruptedForce"; break;
      case Communicator::aborted: s <<             "aborted"; break;
      case Communicator::doExit: s <<              "doExit"; break;
      case Communicator::sleeping: s <<            "sleeping"; break;
      case Communicator::finished: s <<            "finished"; break;
      case Communicator::waiting: s <<             "waiting"; break;
      case Communicator::doReceiveFromMaster: s << "doReceiveFromMaster"; break;
      case Communicator::finishedReceiving: s <<   "finishedReceiving"; break;
      default: s << info[i]->communicator->getState();
    }    
  }
  s << " |";
  out << s.str() << endl;
}


/** Collects the results of finished threads into the _finishedItems array.
 * Also, sets the _isFinished flag for each of the threads;
 * @inv _isFinished[i] == true iff i-th result is in _finishedItems
 */
void MUSExtractionAlgTmp::collectResults(void)
{
  // ok, some threads are done -- we're going to take a snapshot, and merge
  // the results of those threads that are in the finished state at the time
  // of the snapshot; no locking is needed in this case, the workers get into
  // finished state only after they updated their work items, so the work items
  // will be in a good shape; note that even if useSameGID is true, the results
  // might be different: different cores, or different assignments; hence the
  // merging has to be done in any case
  _finishedItems.clear();
  for (int i = 0; i < threads; i++) {
    if (_isFinished[i] = info[i]->isFinished()) { // take the snapshot
      CheckGroupStatus& wi = *(info[i]->workItem);
      if (wi.completed()) { _finishedItems.push_back(&wi); markUnscheduled(wi.gid()); }
      if (masterVerbosity > 1) {
        if (wi.completed())
          cep_mt << "[MASTER] thread " << i
                 <<  " said the group " << wi.gid() << " is "
                 << (wi.status() ? "" : "not") << " necessary" << endl;
        else
          cep_mt << "[MASTER] WARNING: thread " << i
                 << " could not complete SAT check for group " << wi.gid() << endl;
      }
    }
  }
  if (masterVerbosity > 1)
    cep_mt << "[MASTER] " << _finishedItems.size() << " threads have finished." << endl;
}


/** Responsible for analysis and merging of the results of all finished
 * threads. The data for analysis is sitting in the _finishedItems vector;
 * the result will be merged into the global _md. If anything is removed from
 * the global _md, its version number will be incremented.
 *
 * @pre all threads whose data is in _finishedItems are sleeping
 * @pre (TEMP): it is assumed that all entries in _finishedItems are of
 * type CheckGroupStatus; this might change in the future.
 */
void MUSExtractionAlgTmp::mergeResults(void)
{
  // First, let us deal with the UNSAT outcomes (coz then there will be less
  // groups to work through for model rotation).
  //
  // Note that with waitForAll=false we might have some UNSAT outcomes that
  // refer to an older version of MUSData. The easiest thing is just to
  // discard them, however what if the old core is actually a subset of the
  // current instance ? Then it *can* be used for the analysis !
  // TODO: implement this !!!
  //
  // Once we have a set of useful cores, we need to pick one to use --- we're
  // just going to take the smallest one. Is anything smarter possible for
  // certain special cases ?
  //
  size_t max_size = 0;
  CheckGroupStatus* pbest_wi = nullptr;
  _wasted_unsat_outcomes -= _unsat_outcomes;
  for (CheckGroupStatus* pwi : _finishedItems) {
    if (pwi->status()) { continue; }
    if (cbs) { // core-based scheduling
      assert(pwi->pcore() != nullptr);
      for (GID gid : *pwi->pcore()) {
        if (!_md.untested(gid)) { continue; }
        unsigned sc = ++_scores[gid];
        if (sc > _max_score) { _max_score = sc; }
      }
    }
    if (pwi->md().version() < _md.version()) { // old and cannot be used
      bool safe = false;
      if (useOldCores) {
        // we can use this core, if all groups that have been removed since
        // the worker's version are unnecessary from the worker's point of view
        safe = true;
        for (size_t i = _r_vers[pwi->md().version()]; i < _r_vers[_md.version()]; ++i)
          if (!pwi->unnec_gids().count(_r_gids[i])) { safe = false; break; }
      }
      if (!safe) { _wasted_unsat_outcomes++; continue; }
    }
    if (_md.nec(pwi->gid())) {
      // this can actually happen with communication enabled: if someone pushes
      // the negative unit to finalize pwi->gid() while this thread is testing
      // it, then the unit will cause conradiction right away -- in a sence this
      // UNSAT outcome should really have been SAT outcome if not for communiction
      _wasted_sat_outcomes++;
      continue;
    }
    assert(_md.untested(pwi->gid()) &&
          "group should be untested because of the versioning");
    if (pwi->unnec_gids().size() > max_size) {
      pbest_wi = pwi;
      max_size = pbest_wi->unnec_gids().size(); // this just an estimate now,
                                                // because some may have already been removed
    }
    // if someone had a tainted core -- disable rr
    if (pwi->tainted_core()) {
      _tainted_cores++;
      // in adaptive mode, disable redundancy removal
      if (config.get_rm_reda_mode()) {
        // TODO: this is probably wrong (whether or not disable RR can be
        // determined based on the outcomes from all threads).
        pwi->set_use_rr(false);
      }
    }
    _unsat_outcomes++;
  }
  _wasted_unsat_outcomes += _unsat_outcomes - (max_size > 0);
  if (masterVerbosity > 2) {
    ostringstream s;
    s << "unnec_gid sizes: ";
    for (CheckGroupStatus* pwi : _finishedItems) {
      if (!pwi->status())
        s << pwi->unnec_gids().size() << (pwi->tainted_core() ? "(t) " : " ");
    }
    cep_mt << "[MASTER] merging results, " << s.str() << endl;
  }
  if (max_size > 0) { // i.e. someone actually did have UNSAT
    GIDSet& ugids = pbest_wi->unnec_gids();
    unsigned r_gids = 0; // the count of actually removed groups
    // Note that some groups may have been removed earlier, and some groups may
    // have been necessary, and so their units pushed to other solvers in which
    // case they might not necessarially appear in the core
    for (auto gid : ugids) {
      if (!_md.nec(gid) && !_md.r(gid)) {
        _md.mark_removed(gid);
        _sched.update_removed(gid);
        _r_gids.push_back(gid);
        r_gids++;
        if (cbs) { _scores.erase(gid); }
      }
    }
    assert(r_gids > 0 && "at least the GID itself should have been new");
    if (masterVerbosity > 2)
      cep_mt <<  "[MASTER] thread dropping " << r_gids << " unnecessary groups."
             << endl;
    _md.incr_version(); // dropped some groups
    _r_vers.push_back(_r_gids.size()); // update version pointer
    _ref_groups += r_gids - 1;
  }
  // the rest of groups from UNSAT outcomes, if they are not removed, and not
  // already necessary must be re-scheduled
  for (CheckGroupStatus* pwi : _finishedItems) {
     if (pwi->status()) { continue; }
     if (_md.untested(pwi->gid())) { _sched.reschedule(pwi->gid()); }
  }

  // now, for SAT outcomes: simply run model rotation; note that even if the
  // group-ID is the same, the models might be different; but I will not check
  // for this explicitly, since model rotation will *not* go through groups
  // that are known to be necessary, except the starting group. In particular
  // for two identical assignments, there will be only one group checked ---
  // this is much faster than comparing 1M-vectors for equality. The downshot is
  // that this might block two assignments that are different, but not enough to
  // produce different groups after the first flip. TODO: experiment with this.
  RotateModel rm(_md); // item for model rotations
  for (CheckGroupStatus* pwi : _finishedItems) {
    if (!pwi->status()) { continue; }
    // take care of the necessary group: put into MUSData, and mark final
    GID gid = pwi->gid();
    bool is_new = false; // for stats
    if (!_md.nec(gid)) { // this may happen due to another thread
      _md.mark_necessary(gid);
      _sched.update_necessary(gid);
      is_new = true;
      if (cbs) { _scores.erase(gid); }
    }
    // do rotation, if asked for it
    if (config.get_model_rotate_mode()) {
      rm.set_gid(gid);
      rm.set_model(pwi->model());
      rm.set_collect_ft_gids(config.get_reorder_mode());
      _mrotter.process(rm);
      if (rm.completed()) {
        unsigned r_count = 0;
        for (GID gid : rm.nec_gids()) {
          // double-check check if not necessary already and not gid 0
          if (gid && !_md.nec(gid)) {
            _md.mark_necessary(gid);
            _sched.update_necessary(gid);
            r_count++;
            if (cbs) { _scores.erase(gid); }
          }
        }
        if (config.get_reorder_mode()) {
          for (GID gid : rm.ft_gids())
            if (!_md.nec(gid))
              _sched.fasttrack(gid);
          rm.ft_gids().clear();
        }
        _rot_groups += r_count;
        if ((masterVerbosity > 2) && r_count)
          cep_mt <<  "[MASTER] " << r_count
                 << " extra groups are necessary due to rotation." << endl;
        if (!r_count && !is_new) { _wasted_sat_outcomes++; }
      }
      rm.reset();
    }
    _sat_outcomes++;
    if (config.get_rm_reda_mode()) // re-enable redundancy removal
      pwi->set_use_rr(true);
  }
  if (cbs) { // update scheduler
    _sched.clear_fasttrack();
    unsigned count_nec = (cbs == 1) ? threads : 0; // - threads/4 : threads/4;
    unsigned count_unnec = threads - count_nec;
    for (auto& p : _scores)
      if (p.second == _max_score) {
        if (--count_nec >= 0) { _sched.fasttrack(p.first); } else { break; }
      }
    for (auto& p : _scores)
      if (p.second < _max_score) {
        if (--count_unnec >= 0) { _sched.fasttrack(p.first); } else { break; }
      }
  }
}


/** For each of the threads whose _isFinished flag is false (i.e. they were not
 * used for merging) check if it is or was working on a group whose status is
 * already known. If yes, and it is still working, then the thread is aborted.
 * If there's one or more of such threads, the routine blocks until all of them
 * are in finished state.
 * @post _isFinished flag of useless threads is set to true, and the threads are
 * guaranteed to be in finished state
 */
void MUSExtractionAlgTmp::abortUselessThreads(void)
{
  int a_count = 0;
  for (int i = 0; i < threads; i++) {
    if (!_isFinished[i] && !info[i]->isWaiting()) {
      if (masterVerbosity > 2)
          cep_mt << "[MASTER] checking whether to abort thread " << i << " with status " << info[i]->getState() << endl;
      GID gid = info[i]->workItem->gid();
      if (!_md.untested(gid)) {
        if (masterVerbosity > 1)
          cep_mt << "[MASTER] thread " << i << " is on useless group ("
                 << gid << "), aborting" << endl;
	// TODO: Norbert: it is sufficient to signal the "niceEnd" here, and handle the result of that method. Waiting will be done below (in waitForAllAbortedSolvers() )
	// the currently used method waits for each single thread separately. Using "signalNiceEnd()" also increases performance!
	// TODO: get lock of the other thread!!
        if (endSolverNicely(i)) { // it waits until the other guy calls AWAKE, or it returns true
          // already finished, update stats ...
          ++(info[i]->workItem->status() ? _wasted_sat_outcomes : _wasted_unsat_outcomes);
        } else {
          ++a_count;
          ++_aborted_sat_calls;
        }
        _isFinished[i] = true; // will pretend its done
        markUnscheduled(gid);
      }
    }
  }
  if (a_count) { // note that a_count = 0 when waitForAll = true
    masterLock->lock(1350);
    if (masterVerbosity > 1)
      cep_mt << "[MASTER] waiting for " << a_count << " threads to finish nicely." << endl;
    if (masterVerbosity > 2)
      printThreadStates(cep_mt);
    waitForAllAbortedSolvers();
    masterLock->unlock(1360); // Norbert: put here, because the whole method needs to be protected!
  }
}


/** Sets all finished threads (i.e. those with _isFinished[i] == true) into the
 * waiting state, and synchronizes their MUSData instances with the "global"
 * state in _md. On return, all finished threads are ready for processing of the
 * next group.
 */
int MUSExtractionAlgTmp::startWorkerThreads(void)
{
  for( int i = 0 ; i < threads; ++ i ) {
    /* TODO:
     * modify/create new versions of SATChecker, and/or solver factory to be able
     * to include clause sharing (pass another object to all of these tools, thread
     * informations always on extra cache lines!)
     */
    pthread_attr_t attr;
    pthread_attr_init (&attr);
    pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_JOINABLE);
    // create one thread
    int rc = pthread_create( &(threadIDs[i]), &attr, run, (void *)info[i]);
    if (rc) {
      cerr << "[MASTER] return code from pthread_create() is " << rc << " for thread " << i << endl;
      return rc;
    }
    pthread_attr_destroy(&attr);
  } // making threads
  // now, wait for all threads to get into the idle state
  if (masterVerbosity > 2)
    cep_mt <<  "[MASTER] waiting for all threads to become idle" << endl;
  masterLock->lock(1210);
  for (int i = 0; i < threads; i++) {
    while (!info[i]->isIdle())
      masterLock->sleep(1220);
  }
  masterLock->unlock(1230);
  return 0;
}

void MUSExtractionAlgTmp::prepareFinishedThreads(void)
{
  // reset the states of finished/processed threads, and for each thread update
  // its private md structure: for each finished thread we need to figure out the
  // delta between the  current state of master (_md), and each worker's own
  // state; the difference between the two (the sets of removed and finalized
  // groups) should be shoved into worker's md. Important invariant: the group
  // set represented by the master _md is a subset of group-sets represented by
  // slaves, specifically: _md.r_gids() is a superset of worker_md.r_gids(), and
  // _md.nec_gids() is a superset of worker_md.nec_gids().
  for( int i = 0 ; i < threads; ++i ) {
    if (!_isFinished[i]) { continue; }
    assert(info[i]->isFinished() && "the thread has to be finished" );
    // write -> lock the worker state!
    info[i]->ownLock->lock(1370); // if master can go here, worker thread sleeps
    info[i]->setStateM( ThreadInfo::waiting );
    info[i]->ownLock->unlock(1380);
    // update worker's md
    update_state(i);
    DBG(cout << "Deltas for thread " << i << " r_list={ ";
        for (GID gid : info[i]->md->r_list()) { cout << gid << " "; }
        cout << "}, f_list={ ";
        for (GID gid : info[i]->md->f_list()) { cout << gid << " "; }
        cout << "}" << endl;);
  }
}

/** Resposible for updating the state of the worker (worker_md) to match
 * the state of the master (this->_md). Note that it also updates the
 * version of the worker.
 * @inv: _md is a "subset" of worker_md (in terms of group set)
 * @post: worker_md.version() == _md.version();
 */
void MUSExtractionAlgTmp::update_state(int thread)
{
  MUSData& worker_md = *info[thread]->md;
  CHK(cout << "WARNING: expensive invariant checks in update_state()" << endl;
      bool passed = true;
      if (!includes(_md.r_gids().begin(), _md.r_gids().end(),
                    worker_md.r_gids().begin(), worker_md.r_gids().end())) {
        cout << "ERROR: worker's set of removed groups is not a subset of master's"
             << endl;
        passed = false;
      }
      if (!includes(_md.nec_gids().begin(), _md.nec_gids().end(),
                    worker_md.nec_gids().begin(), worker_md.nec_gids().end())) {
        cout << "ERROR: worker's set of necessary groups is not a subset of master's"
             << endl;
        passed = false;
      }
      if (!passed) { exit(-1); });
  // removed groups -- these are all groups from worker's version to master's
  worker_md.r_list().clear();
  for (size_t i = _r_vers[worker_md.version()]; i < _r_vers[_md.version()]; ++i) {
    worker_md.r_list().push_back(_r_gids[i]);
    worker_md.r_gids().insert(_r_gids[i]);
  }
  worker_md.set_version(_md.version());
  // necessary groups
  // TODO: in principle, we should set up the same versioning deal with
  // necessary groups, to avoid doing the set difference; however my profiling
  // showed that the time spent here is insignificant (probably due to the
  // fact that there are relatively few necessary groups in the first place).
  worker_md.f_list().clear();
  set_difference(_md.nec_gids().begin(), _md.nec_gids().end(),
                 worker_md.nec_gids().begin(), worker_md.nec_gids().end(),
                 back_inserter(worker_md.f_list()));
  worker_md.nec_gids().insert(worker_md.f_list().begin(), worker_md.f_list().end());
  // version
}



/** Computes and prints out all the algorithm-specific statistics
 */
void MUSExtractionAlgTmp::computeAndDumpStats(void)
{
  // after all work has been done, sync the solvers, extract statistics
  unsigned rej_sent_size = 0;
  unsigned rej_sent_lbd = 0;
  unsigned rej_rcv_stamp = 0;
  for (int i = 0; i < threads; i++) {
    SATChecker* sc = info[i]->_schecker;
    _sat_calls += sc->sat_calls();
    _sat_time += sc->sat_time();
    _sent_cls += info[i]->communicator->nrSendCls;
    _rcvd_cls += info[i]->communicator->nrReceivedCls;
    rej_sent_size += info[i]->communicator->nrRejectSendSizeCls;
    rej_sent_lbd += info[i]->communicator->nrRejectSendLbdCls;
    rej_rcv_stamp += info[i]->communicator->nrRejectRcvStamp;
  }
  if (config.get_verbosity() > 0) {
    cep_mt << "[MASTER] stats: " << endl;
    cout_pref << "  SAT calls total: " << _sat_calls << endl;
    cout_pref << "  SAT calls per thread: ";
    for (int i = 0; i < threads; i++)
      cout << info[i]->_schecker->sat_calls() << " ";
    cout << endl;
    cout_pref << "  SAT time total: " << _sat_time << " sec" << endl;
    cout_pref << "  SAT time per thread: ";
    for (int i = 0; i < threads; i++)
      cout << info[i]->_schecker->sat_time() << " ";
    cout << endl;
    cout_pref << "  SAT outcomes: " << _sat_outcomes << endl;
    cout_pref << "  UNSAT outcomes: " << _unsat_outcomes << endl;
    cout_pref << "  wasted SAT outcomes: " << _wasted_sat_outcomes << endl;
    cout_pref << "  wasted UNSAT outcomes: " << _wasted_unsat_outcomes << endl;
    cout_pref << "  aborted SAT calls: " << _aborted_sat_calls << endl;
    cout_pref << "  rot. points: " << _mrotter.num_points() << endl;
    cout_pref << "  tainted cores: " << _tainted_cores << endl;
    cout_pref << "  avg. received clauses: " << _rcvd_cls/threads << endl;
    cout_pref << "  avg. sent clauses: " << _sent_cls/threads << endl;
    cout_pref << "  avg. rejected sends due to size: " << rej_sent_size/threads << endl;
    cout_pref << "  avg. rejected sends due to LBD: " << rej_sent_lbd/threads << endl;
    cout_pref << "  avg. rejected recvs due to stamp: " << rej_rcv_stamp/threads << endl;
    cout_pref << "  comm receives/received/sent/rej_sent_size/rej_sent_lbd/rej_rcv_stamp per thread: ";
    for (int i = 0; i < threads; i++) {
      Communicator* comm = info[i]->communicator;
      cout << comm->nrReceive << "/" << comm->nrReceivedCls << "/" 
           << comm->nrSendCls << "/"
           << comm->nrRejectSendSizeCls << "/"
           << comm->nrRejectSendLbdCls << "/"
           << comm->nrRejectRcvStamp << " ";
    }
    cout << endl;
  }
}


//
// local implementations ....
//

namespace {

  /** send interrupt signal to all threads!
   *  should be called by the master only!
   */
  void sendInterruptToAllThreads(ThreadInfo** info, int threads)
  {
    //bool done[threads]; // ANTON: what is this for ? its never read
    //for( int i = 0 ; i < threads; ++ i ) done[i] = false;

    /** tell all threads, that they should interrupt their work!
     */
    for( int i = 0; i < threads; ++ i )
    {
      assert( !info[i]->isWaiting() && "thread cannot be waiting when master wants to send more data" );
      //if( info[i]->isFinished() ) { done[i] = true; continue; } // finished, no need to wait for this thread!
      //if( ! info[i]->communicator->isWorking() ) { done[i] = true; continue; } // not working, no
      assert( !info[i]->communicator->isAborted() && "do not overwrite abort state here!!" );
      info[i]->communicator->setState( Communicator::interrupted );
      // no need to wakeup thread here!
    }
  }

  /** Wait until all threads are either finished or "not-working" (in communication)
   *  should be called by the master only!
   *  Note: it is assumed, that master already grabed its lock!!
   */
  void waitForAllThreadsBeingInterrupted(ThreadInfo** info, SleepLock* masterLock, int threads)
  {
    // NOTE: master should have the lock!
    bool done[threads];
    for( int i = 0 ; i < threads; ++ i ) done[i] = false;
    bool allSleep = false;
    while ( !allSleep )
    {
      masterLock->sleep(1390); // wait until somebody called awake()
      int i = 0;
      for(  ; i < threads; ++ i )
      {
        if( done[i] ) continue;
        // in the meantime, the thread could have finished its work, or it reached the updateSleep-method in the search
        if( info[i]->isFinished() ) { done[i] = true; continue; } // finished, no need to wait for this thread!
        if( ! info[i]->communicator->isWorking() ) { done[i] = true; } // not working,
      }
      if( i == threads ) allSleep = true;
    }
    // NOTE: master still has its lock!
  }

  /** intterupt all threads based on communication (within the solver)
   *  should be called by the master only!
   *  Note: returns, if all solver have either left search already, or until they are interrupted inside their search loop
   *  Note: it is assumed, that master already grabed its lock!!
   */
  void interruptAllThreads( ThreadInfo** info, SleepLock* masterLock, int threads )
  {
    // NOTE: it is assumed, that master already grabed its lock!

    // send interrupt to everybody!
    sendInterruptToAllThreads(info, threads);
    // ensure that all threads are waiting
    waitForAllThreadsBeingInterrupted(info,masterLock,threads);
    // NOTE: master still has its lock!
  }

  /** let all solvers (except finished ones) receive the clauses!
   * assumption 1: masterlock is grabed
   * assumptino 2: each solver is waiting (either finished, or within the sleep loop! (updateSleep-method))
   */
  void letSolversReceiveMasterUnits(ThreadInfo** info, SleepLock* masterLock, int threads)
  {
    bool done[threads];
    for( int i = 0 ; i < threads; ++ i ) done[i] = false;

    /** tell all threads, that they should receive master unit clauses
     */
    for( int i ; i < threads; ++ i )
    {
      assert( !info[i]->isWaiting() && "thread cannot be waiting when master wants to send more data" );
      assert( !info[i]->communicator->isWorking() && "thread cannot be searching when master wants to send data");
      if( info[i]->isFinished() ) { done[i] = true; continue; } // finished, no need to wait for this thread!
      info[i]->communicator->setState( Communicator::doReceiveFromMaster );
      info[i]->communicator->ownLock->awake(1400); // wake up thread!
      // todo: continue here!
    }

    // all threads have the state: doReceive

    bool allSleep = false;
    while ( !allSleep )
    {
      int i = 0;
      for(  ; i < threads; ++ i )
      {
        if( done[i] ) continue;
        // in the meantime, the thread could have finished its work, or it reached the updateSleep-method in the search
        if( info[i]->isFinished() ) { done[i] = true; continue; } // finished, no need to wait for this thread!
        if( info[i]->communicator->isWaiting() ) {
          info[i]->communicator->setState(Communicator::doReceiveFromMaster);
          // in the next iteration, the thread should receive the clauses and set its state to "finishedReceiving"
          info[i]->ownLock->awake(1410);
        } else if( info[i]->communicator->isFinishedReceiving() ) {
          done[i] = true; // this thread reveiced the clauses!!
          info[i]->communicator->setState(Communicator::working);
          info[i]->ownLock->awake(1420); // from here, thread is running as usual!!
        }
      }
      if( i == threads ) allSleep = true;
      else masterLock->sleep(1430); // wait until somebody called awake()
    }
  }

}

/**----------------------------------------------------------------------------*/


