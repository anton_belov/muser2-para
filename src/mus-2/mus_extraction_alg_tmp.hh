/*----------------------------------------------------------------------------*\
 * File:        mus_extraction_alg_tmp.hh
 *
 * Description: Class definition of the parallel MUS extraction algorithm.
 *
 * Author:      antonb, norbert
 * 
 * Notes:
 *
 *                          Copyright (c) 2012-13, Anton Belov, Norbert Manthley
\*----------------------------------------------------------------------------*/

#pragma once

#include <pthread.h>
#include <vector>
#include "group_scheduler.hh"
#include "model_rotator.hh"
#include "mus_config.hh"
#include "mus_data.hh"
#include "sat_checker.hh"
#include "mus_extraction_alg.hh"

#define ce cerr << "c [MUS] "

/** forward declare some classes to avoid unnecessary includes */
class ThreadInfo;
class SleepLock;
class CommunicationData;

/** This is the implementation of an experimental MUS extraction algorithm.
 */
class MUSExtractionAlgTmp : public MUSExtractionAlg {
  
public:
  
  MUSExtractionAlgTmp(IDManager& imgr, ToolConfig& conf, SATChecker& sc, 
                       ModelRotator& mr, MUSData& md, GroupScheduler& s) 
    : MUSExtractionAlg(imgr, conf, sc, mr, md, s) {}

  /** The main extraction logic is implemented here.
   */
  void operator()(void);

private:

  // key datastructures

  int threads = 0;                 // number of worker threads

  SleepLock* masterLock = nullptr; // the lock for master thread

  ThreadInfo** info = nullptr;     // info about the tasks that should be executed in parallel

  CommunicationData* communicationData = nullptr; // clause buffer for communication among all threads

  pthread_t* threadIDs = nullptr;  // posix thread ID of worker threads

  // the following two vectors are used to keep track of removed groups for each
  // version; r_gids[0, r_vers[i]) is the list of groups removed in the version
  // version i of the master MUSData _md
  std::vector<GID> _r_gids;
  std::vector<size_t> _r_vers;

  // support for core-based-scheduling
  std::unordered_map<GID, unsigned> _scores; // score for each group: how many times its in a core
  unsigned _max_score = 0;

  // options

  bool useSameGID = false;         // all threads process the same group ID

  bool waitForAll = false;         // proceed with the evaluation only after all SAT solvers
                                   // finished their current call

  int masterVerbosity = 0;         // verbosity

  bool useOldCores = false;        // when true, make an attempt to use outdated cores

  int cbs = 0;                     // core-based-scheduling: 0 - disabled,
                                   // 1 - prioritize necessary, 2 - prio. unnecessary

  // thread control ...

  /** Initializes the ThreadInfo structures of the worker threads
   */
  void initWorkerThreads(void);

  /** Releases all the memory allcated in initWorkerThreads (TODO)
   */
  void deinitWorkerThreads(void);

  /** Starts the worker threads, and waits until they get into the idle state.
   * The IDs of created threads are recorded in threadIDs.
   * @return 0 on success, UNIX errno in case of failure
   */
  int startWorkerThreads(void);

  /** Terminates all the worker threads (TODO)
   */
  void stopWorkerThreads(void);

  /** Starts all currently idle (or waiting) threads. The first thread is started
   * to work on the group _nextUnscheduledGroup; the rest - depending on useSameGID
   * parameter.
   * Notes:
   *   - should be called by the master only
   *   - assumes that masterLock is not owned at the time of the call
   *   - masterLock will be owned on return
   */
  void startIdleThreads(void);

  /** Waits for *some* thread to finish, i.e. to get into the finished state.
   * Returns the index of the thread that finished, or threads in case of error.
   * Note:
   *   - should be called by the master only
   *   - assumes that masterLock is owned
   */
  int waitForSomeThreadToFinish(void);

  /** Waits for *all* threads to finish, i.e. to get into the finished state.
   * Returns the index of the thread that finished, or -1 in case of error.
   * Note:
   *   - should be called by the master only
   *   - assumes that masterLock is owned
   */
  void waitForAllThreadsToFinish(void);

  /** kill a given solver nicely
   * @param info ThreadInfo array
   * @param thread the index of the thread to terminate
   * @param masterLock pointer to the lock of the master thread
   * @return true, if thread has been finished before check
   * Details: here, that caller waits directly for the solver to finish
   * from a run time point of view it would be better to give the thread some time to finish!
   * by using signalNiceEnd and waitForAllAbortedSolvers
   * Note: 
   *  - should be called by the master only!
   *  - assumes that master-lock is not grabbed!
   *  - after this method has been called, state of other solvers might have changed as well!
   */
  bool endSolverNicely(int thread);
  
  /** as method endSolverNicely tells solver to abort
   * @return true, if solver finished already (in this case, state of the solver is not changed!)
   */
  bool signalNiceEnd(int thread);
  
  /** return after all threads which are marked as aborted are also finished
   *  Note:
   *   - should be called by the master only
   *   - assumes that masterLock is grabbed already
   */
  void waitForAllAbortedSolvers(void);
  
  /** Returns true if all threads are in the waiting state. This is just a quick
   * check, without any locking */
  bool allThreadsAreWaiting(void);

  /** prints the state of the threads to the output stream */
  void printThreadStates(std::ostream& out);

  // support for "clean" access to unscheduled groups
  GID _nextUnscheduledGroup = gid_Undef;
  GIDSet _schedGroups;  // currently scheduled groups
  /** returns true if there's an unscheduled group */
  bool hasUnscheduledGroups(void) {
    while (_sched.next_group(_nextUnscheduledGroup)) {
      if (_schedGroups.count(_nextUnscheduledGroup)) { continue; }
      if (_md.untested(_nextUnscheduledGroup)) { return true; }
    }
    _nextUnscheduledGroup = gid_Undef;
    return false;
  }
  /** marks group as currently scheduled */
  void markScheduled(GID gid) { _schedGroups.insert(gid); }
  /** unmarks group as scheduled (i.e. can be re-scheduled if needed) */
  void markUnscheduled(GID gid) { _schedGroups.erase(gid); }

  // support for analysis and merging of results from different threads
  // TEMP: for now all finished work items are assumed to be CheckGroupStatus
  // this might change in the future versions.

  std::vector<bool> _isFinished;                  // true if thread i is done

  std::vector<CheckGroupStatus*> _finishedItems;  // workitems of finished threads

  /** Collects the results of finished threads into the _finishedItems array.
   * Also, sets the _isFinished flag for each of the threads;
   * @inv _isFinished[i] == true iff i-th result is in _finishedItems
   */
  void collectResults(void);

  /** Responsible for analysis and merging of the results of all finished
   * threads. The data for analysis is sitting in the _finishedItems vector;
   * the result will be merged into the global _md. If anything is removed from
   * the global _md, its version number will be incremented.
   *
   * @pre all threads whose data is in _finishedItems are sleeping
   * @pre (TEMP): it is assumed that all entries in _finishedItems are of
   * type CheckGroupStatus; this might change in the future.
   */
  void mergeResults(void);

  /** For each of the threads whose _isFinished flag is false (i.e. they were not
   * used for merging) check if it is or was working on a group whose status is
   * already known. If yes, and it is still working, then the thread is aborted.
   * If there's one or more of such threads, the routine blocks until all of them
   * are in finished state.
   * @post _isFinished flag of useless threads is set to true, and the threads are
   * guaranteed to be in finished state
   */
  void abortUselessThreads(void);

  /** Sets all finished threads (i.e. those with _isFinished[i] == true) into the
   * waiting state, and synchronizes their MUSData instances with the "global"
   * state in _md. On return, all finished threads are ready for processing of the
   * next group.
   */
  void prepareFinishedThreads(void);

  /** Resposible for updating the MUSData of the worker thread to match
   * the state of the master (this->_md). Note that it also updates the
   * version of the worker.
   * @inv: _md is a "subset" of worker's _md (in terms of group set)
   * @post: worker's _md.version() == _md.version();
   */
  void update_state(int thread);

  // additional stats

  int _wasted_unsat_outcomes = 0;  // number of unused UNSAT outcomes

  int _wasted_sat_outcomes = 0;    // number of unused SAT outcomes

  int _aborted_sat_calls = 0;      // number of aborted SAT calls

  unsigned _sent_cls = 0;          // total number of sent clauses

  unsigned _rcvd_cls = 0;          // total number of received clauses

  /** Computes and prints out all the algorithm-specific statistics
   */
  void computeAndDumpStats(void);

};

/*----------------------------------------------------------------------------*/
