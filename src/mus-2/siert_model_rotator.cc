/*----------------------------------------------------------------------------*\
 * File:        recursive_model_rotator.cc
 *
 * Description: Implementation of model rotator that uses RMR (FMCAD-2011) with
 *              the optimization proposed by Siert Wieringa (CP-12), plus an
 *              extra depth parameter (AICOMM-11)
 *
 * Author:      antonb
 * 
 * Notes:
 *
 *
 *                                              Copyright (c) 2013, Anton Belov
 \*----------------------------------------------------------------------------*/

#include <cassert>
#include <unordered_map>
#include "model_rotator.hh"

using namespace std;
using namespace __gnu_cxx;

//#define DBG(x) x

namespace {

} // anonymous namespace

/* Decides whether or not to rotate through a group on a specified literal;
 * assumes that group has been already determined necessary
 */
bool SiertModelRotator::rotate_through(RotateModel& rm, GID gid, LINT lit)
{
  group_map::iterator pm = _gm.find(gid);
  if (pm == _gm.end()) { pm = _gm.emplace(make_pair(gid, lit_count_map())).first; }
  lit_count_map& lm = pm->second;
  lit_count_map::iterator pl = lm.find(lit);
  if (pl == lm.end()) { pl = lm.emplace(make_pair(lit, 0)).first; }
  unsigned vc = ++pl->second;
  DBG(cout << "gid=" << gid << ", lit=" << lit << ", vc = " << vc << endl;);
  return (vc <= _depth);
}

//
// ------------------------  Local implementations  ----------------------------
//

namespace {

} // anonymous namespace
