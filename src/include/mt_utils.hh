/**----------------------------------------------------------------------------*\
 * File:        mt_utils.hh
 *
 * Description: A kitchen sink for various MT-related utilities
 *
 * Author:      antonb, norbert
 * 
 * Notes:
 *
 *                          Copyright (c) 2011-12, Anton Belov, Norbert Manthey
\*----------------------------------------------------------------------------*/

#include <pthread.h>
#include <iostream>

namespace MTUtils {

  /** Provides "one-shot" output which is better for mutlithreaded logging and
   *  debugging. Note that there's an overhead and an explicit serialization of
   *  access to cout/cerr, so use judiciously
   */
  class out_mt {
  public:
    out_mt(std::ostream& out, const char* pref = "") 
      : _out(out), _pref(pref) {}
    ~out_mt(void) {
      _buff.flush();
      _out << _pref << _buff.str() << flush;
    }
    std::ostringstream& get(void) {
      _buff << "[tid " << hex << ::pthread_self() << dec << "] " << dec;
      return _buff;
    }
  private:
    std::ostream& _out;
    std::ostringstream _buff;
    const char* _pref;
  };

}

// macros to be used as a replacement for cout, cout_pref, cerr
#define co_mt (MTUtils::out_mt(std::cout).get())
#define cop_mt (MTUtils::out_mt(std::cout, "c ").get())
#define ce_mt (MTUtils::out_mt(std::cerr).get())
#define cep_mt (MTUtils::out_mt(std::cerr, "c ").get())



