
#ifndef PARALLEL_HH
#define PARALLEL_HH

#include <cstdio>
#include <iostream>
#include <queue>
#include <signal.h>
#include <stack>
#include <unistd.h>


// for parallel stuff
#include <pthread.h>


/** locking class, also for waiting 
 * class that offers a mutex combibed with a conditional variable and a boolean variable
 */
class SleepLock
{
  // bool sleeps;               /// is set to true, iff last time somebody called sleep() before awake was called
  pthread_mutex_t mutex;     /// mutex for the lock
  pthread_cond_t master_cv;  /// conditional variable for the lock
  
  // do not allow the outside to copy this lock
  explicit SleepLock(const SleepLock& l )
  {};
  SleepLock& operator=(const SleepLock& l)
  {return *this;}
  
public:
  /** setup the lock
   * @param initialSleep first call to sleep will lead to sleep if the parameter is true (if awake is not called inbetween)
   */
  SleepLock() // : sleeps( initialSleep )
  {
    pthread_mutex_init(&mutex,     0);
    pthread_cond_init (&master_cv, 0);
  }
  
  ~SleepLock()
  {
    // sleeps = false;
    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy (&master_cv);
  }
  
  /// get the lock
  void lock(){
    pthread_mutex_lock (&mutex); 
  }
  
  /// release the lock
  void unlock(){
    pthread_mutex_unlock (&mutex); 
  }
    
  
  /** sleep until somebody calls awake()
   *  Note: should only be called between the lock() and unlock() command!
   *        After waking up again, unlock() has to be called again!
   */
  void sleep(){
    pthread_cond_wait (&master_cv,&mutex); // otherwise sleep now!
  }
  
  /** wakeup all sleeping threads!
   * @param isLocked lock is already locked by calling thread
   *  Note: waits until it can get the lock
   *        wakes up threads afterwards (cannot run, because calling thread still has the lock)
   *        releases the lock
   */
  void awake(bool isLocked = false)
  {
    if( !isLocked ) pthread_mutex_lock (&mutex);
    pthread_cond_broadcast (&master_cv); // initial attempt will fail!
    if( !isLocked ) pthread_mutex_unlock (&mutex); 
  }
};


#endif