
#ifndef PARSING_HH
#define PARSING_HH

#include <cstdio>
#include <iostream>
#include <queue>
#include <signal.h>
#include <stack>
#include <unistd.h>

#include "basic_group_set.hh"
#include "globals.hh"
#include "id_manager.hh"
#include "mus_config.hh"
#include "mus_data.hh"
#include "mus_extractor.hh"
#include "test_mus.hh"
#include "tester.hh"

#include "../../src/parse/cnffmt/cnffmt.hh"
#include "../../src/parse/gcnffmt/gcnffmt.hh"
#include "../../src/tools/muser-2/toolcfg.hh"


/** Loads input into group set */
void load_file(const char* fname, ToolConfig& config, IDManager& imgr,
                 BasicGroupSet& clset);


#endif