/*********************************************************************************[Communication.h]
Copyright (c) 2012, All rights reserved, Norbert Manthey

 **************************************************************************************************/

#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include <cmath>
#include <deque>
#include <vector>

#include <iostream>

using namespace std;

// own files
#include "LockCollection.h"
//#include "Controller.h"
// minisat files
#include "../extsrc/minisat22-2/core/Solver.h"

#define MYDBG(x)

/** ringbuffer that can be used to share clauses among multiple solver incarnations
 * note: is build based on MiniSATs Lit and Vec structures
 */
class ClauseRingBuffer
{
  /** item for the pool, remembers the sender so that own clauses are not received again
   */
  struct poolItem {
    std::vector<Minisat::Lit> data;	/// the actual clause
    int author;		/// the author of the clause (low 8 bit) | stamp
  };

  Lock dataLock;		/// lock that protects the access to the task data structures
  poolItem* pool;		/// ringbuffer for the clauses
  unsigned poolSize;		/// size of the pool
  unsigned addHereNext;		/// index of the position where the next clause will be added in the buffer


  /** get the author of the clause of the given position in the pool
   * @param position index of the clause that should be received
   * note: this method should be locked
   */
  int getAuthor( const unsigned position ) {
    return pool[position].author;
  }

  /** get the clause of the given position to the pool
   * @param position index of the clause that should be received
   * @param allocator clause allocator of the solver that receives clauses (clauses are copied directly into the allocator
   * note: this method should be locked
   */
  Minisat::CRef getClause( const unsigned position, Minisat::ClauseAllocator& allocator )
  {
    std::vector<Minisat::Lit>& poolClause = pool[position].data;
    return allocator.alloc(poolClause, true); // create as learned clause!!
  }

  /** lock the whole data set
   */
  void lock() {
    dataLock.lock();
  }

  /** unlock the whole data set
   */
  void unlock() {
    dataLock.unlock();
  }

public:

  /** create the data that is needed for adding enough clauses
   */
  ClauseRingBuffer( const unsigned size )
  :
    poolSize(size),
    addHereNext(1) // initially, last seen is set to 0 for each thread. to not incorporate non-initialized clauses, start at 1
  {
    pool = new poolItem [size];
    poolSize = size;
  }

  ~ClauseRingBuffer()
  {

  }

  unsigned size() const { return poolSize; }
  
  /** return the position of the clause that has been deleted last
   */
  unsigned getCurrentPosition() const { return ((addHereNext == 0) ? poolSize - 1 : addHereNext - 1); }

  /** adds a clause to the next position of the pool
   * @param authorID id of the author thread, to be stored with the clause
   * @param clause vector that stores the clause to be added
   */
  void addClause( int authorID, const Minisat::vec<Minisat::Lit>& clause )
  {
    lock();

    // cerr << "[COMM] thread " << authorID << " adds clause to " << addHereNext << endl;
    MYDBG(cout << "[COMM] thread " << (authorID&0xff) << " adds a clause with stamp " << (authorID>>8) << endl;);
    // overwrite current position (starts with 0)
    pool[addHereNext].author = authorID;
    std::vector<Minisat::Lit>& poolClause = pool[addHereNext].data;
    poolClause.resize( clause.size() );
    for( int i = 0 ; i < clause.size(); ++i ) poolClause[i] = clause[i];

    // push pointer to the next position
    // stay in the pool!
    addHereNext ++;
    addHereNext = ( addHereNext == poolSize ? 0 : addHereNext );
    
    unlock();
  }
  
  /** adds a set of unit clauses to the pool
   * @param authorID id of the author thread, to be stored with the clause
   * @param units vector that stores all the literals that are inside the unit clauses to share
   */
  void addUnitClauses( int authorID, const std::vector<Minisat::Lit>& units )
  {
    lock(); // coarse lock, do not lock for each unit, but for all!

    for( size_t i = 0 ; i < units.size(); ++ i ) {
      // cerr << "[COMM] thread " << authorID << " adds clause to " << addHereNext << endl;
      MYDBG(cout << "[COMM] thread " << (authorID&0xff) << " adds a unit with stamp " << (authorID>>8) << endl;);
      // overwrite current position (starts with 0)
      pool[addHereNext].author = authorID;
      std::vector<Minisat::Lit>& poolClause = pool[addHereNext].data;
      poolClause.resize( 1 );
      poolClause[0] = units[i];

      // push pointer to the next position
      // stay in the pool!
      addHereNext ++;
      addHereNext = ( addHereNext == poolSize ? 0 : addHereNext );
    }
    unlock();
  }
  
  /** copy all clauses into the clauses vector that have been received since the last call to this method
   * @param authorID id of the author thread, to be stored with the clause
   * note: only an approximation
   */
  unsigned receiveClauses ( int authorID, unsigned lastSeenIndex, Minisat::ClauseAllocator& allocator, std::vector< Minisat::CRef>& clauses, unsigned& rejCnt)
  {
    //cerr << "c [COMM] thread " << authorID << " called receive with last seen " << lastSeenIndex << ", addHere: " << addHereNext << endl;
    clauses.clear();
    std::vector<Minisat::Lit> tmp;
    lock();
    // incorporate all clauses that are stored BEFORE addHereNext
    unsigned returnIndex = addHereNext == 0 ? poolSize - 1 : addHereNext - 1;

    const unsigned startIndex = lastSeenIndex == poolSize - 1 ? 0 : lastSeenIndex + 1; // first clause that needs to be copied
    const unsigned stopIndex = addHereNext;     // last clause that needs to be copied

    //cerr << "c [COMM] thread " << authorID << " start:" << startIndex << " stop:" << stopIndex << " return: " << returnIndex << endl;

    // fix up
    int stamp = authorID >> 8;
    authorID &= 0xff;
    MYDBG(cout << "[COMM] thread " << authorID << " receiving clauses, current stamp is " << stamp << endl;);

    // do not copy anything, if the next position is the one where the next clause would be added
    if( startIndex != addHereNext )
    {
      if( startIndex < stopIndex ) {
        for( unsigned i = startIndex; i < stopIndex; ++ i ) { // do copy the last clause!
          // receive only, if calling thread was not the author
          int auth = getAuthor(i);
          if( (auth & 0xff) != authorID ) {
            //cerr << "[COMM] c try to get clause from " << i << endl;
            if ((auth >> 8) <= stamp) {
              clauses.push_back( getClause(i, allocator) ); // create clause directly in clause allocator
              tmp.clear();
            } else {
              ++rejCnt;
              MYDBG(cout << "[COMM] rejecting clause with higher stamp " << (auth >> 8) << endl;);
            }
          }
        }
      } else { // startIndex > stopIndex
        for( unsigned i = startIndex; i < poolSize; ++i ) {
          // receive only, if calling thread was not the author
          int auth = getAuthor(i);
          if( (auth & 0xff) != authorID ) {
            //cerr << "[COMM] c try to get clause from " << i << endl;
            if ((auth >> 8) <= stamp) {
              clauses.push_back( getClause(i, allocator) ); // create clause directly in clause allocator
              tmp.clear();
            } else {
              ++rejCnt;
              MYDBG(cout << "[COMM] rejecting clause with higher stamp " << (auth >> 8) << endl;)
            }
          }
        }
        for( unsigned i = 0 ; i < stopIndex; ++ i ) {
          // receive only, if calling thread was not the author
          int auth = getAuthor(i);
          if( (auth & 0xff) != authorID ) {
            //cerr << "[COMM] c try to get clause from " << i << endl;
            if ((auth >> 8) <= stamp) {
              clauses.push_back( getClause(i, allocator) ); // create clause directly in clause allocator
              tmp.clear();
            } else {
              ++rejCnt;
              MYDBG(cout << "[COMM] rejecting clause with higher stamp " << (auth >> 8) << endl;);
            }
          }
        }
      }
    } else { // end if (something to share)
      //cerr << "[COMM] c thread " << authorID << " nothing new to import" << endl;
    }
    unlock();
    return returnIndex;
  }

  };

  /** object that takes care which data is shared among the threads, handles
   */
  class CommunicationData
  {
    ClauseRingBuffer ringbuffer; /// buffer that stores the shared clauses

    Lock dataLock;		/// lock that protects the access to the task data structures
    SleepLock masterLock;		/// lock that enables the master thread to sleep during waiting for child threads

    Minisat::vec <Minisat::Lit> sendUnits;	/// vector that stores the unit clauses that should be send to all clients as clauses (not learned!)

  public:

    CommunicationData () :
      ringbuffer(1024)
    {

    }

    SleepLock& getMasterLock() { return masterLock; };


    /** return a reference to the ringbuffer
     */
    ClauseRingBuffer& getBuffer() { return ringbuffer; }

    /** clears the vector of units to send
     * should be called by the master thread only!
     */
    void clearToSend() {
      sendUnits.clear();
    }

    /** adds the given literal to the vector of literals that should be sent
     * should be called by the master thread only
     */
    void addToSendThisUnit( int unitLiteral ) {
      // convert into literal, push to vector
      sendUnits.push(Minisat::mkLit(abs(unitLiteral),unitLiteral < 0));
    }

    /** receive clauses
     * should be called by worker threads
     * @param fillMe vector of the client that should store the literals that have been send recently
     */
    void receiveUnits( Minisat::vec<Minisat::Lit>& fillMe ) {
      fillMe.clear();
      for( int i = 0 ; i < sendUnits.size(); ++i )
        fillMe.push( sendUnits[i] );
      cout << "ERROR: receiveUnits should not be called !" << endl;
      exit(-1);
    }
  };

  /** provide the major communication between thread and master!
   */
  class Communicator {
  public:
    // attributes

    SleepLock* ownLock;		// sleep lock of this thread to not waste cpu time

    CommunicationData* data;	// pointer to the data, that is shared among all threads

    // TODO: think about read and write. master writes, client polls, could set back to poll again
    enum State {
      idle, // has no work at the moment
      working,// is simply working
      interrupted,// interrupt current run, proceed with the run afterwards!
      interruptedForce, // interrupted with force (to perform a restart immedeately)
      aborted,// abort current solving run and wait for next work item
      doExit, // at next interruption thread is canceled
      sleeping, // the thread currently sleeps
      finished, // thread finished its work on the current group
      waiting,  // thread waits and master does something with it
      doReceiveFromMaster, // thread should receive shared unit clauses (from master)
      finishedReceiving,   // thread is finished with receiving!
    };

  private:

    Minisat::Solver * solver;  // pointer to the used solver object
    int id;                    // id of this thread
    int stamp;

    State state;

    int myLastTaskID;
    unsigned lastSeenIndex;    // position of the last clause that has been incorporated
    bool doSend;               // should this thread send clauses
    bool doReceive;            // should this thread receive clauses

    Minisat::vec<char> protect;         // if char in vector is 0, the variable has to be considered for calculating limits
    
    char dummy[64]; // to separate this data on extra cache lines (avoids false sharing)

    // methods
  public:
    // seet default values, ownLock is set to initial sleep
    Communicator(const int id, CommunicationData* communicationData) :
      ownLock( new SleepLock() )
    ,data( communicationData) 
    ,solver(0) 
    ,id(id) 
    ,stamp(0)
    ,state(idle) 
    ,myLastTaskID(-1) 
    ,lastSeenIndex(0)
    ,doSend(true)               // should this thread send clauses
    ,doReceive(true)
    ,protectAssumptions(false)  // should the size limit check also consider assumed variables?
    ,sendSize   (10)  // initial value, also minimum limit (smaller clauses can be shared if LBD is also accepted) 
    ,sendLbd    ( 5)  // initial value, also minimum limit (smaller clauses can be shared if size is also accepted) 
    ,sendMaxSize(128) // upper bound for clause size (larger clause is never shared)
    ,sendMaxLbd (32)  // upper bound for clause lbd (larger lbd is never shared) 
    ,sizeChange (0.0) // TODO: set to value greater than 0 to see dynamic limit changes! (e.g. 0.05)
    ,lbdChange  (0.0) // TODO: set to value greater than 0 to see dynamic limit changes! (e.g. 0.02)
    ,sendRatio  (0.1) // how many of the learned clauses should be shared? 10%?
    ,doBumpClauseActivity(false)
    ,nrSendCls (0)      
    ,nrRejectSendSizeCls(0)
    ,nrRejectSendLbdCls(0)
    ,nrReceivedCls(0)
    ,nrReceive(0)
    ,receiveEvery(128)
    ,nrRejectRcvStamp(0)
    {
      // do create the solver here, or from the outside?
      // solver = new Solver();
    }

    // destructor
    ~Communicator()
    {
      if( ownLock != 0 ) delete ownLock;
    }

    void setSolver( Minisat::Solver* s ) {
      assert( solver == 0 && "will not overwrite handle to another solver" );
      solver = s;
    }

    State getState() const {
      return state;
    }

    const bool getDoSend() { return doSend; }
    void setDoSend(bool ds) { doSend = ds; }
    const bool getDoReceive() { return doReceive; }
    void setDoReceive(bool dr) { doReceive = dr; }

    /** update the state of the thread (define what will happen next)
     * @param s future state
     * note: when this method is called, the lock ownlock should be locked first!
     * @return true, if state transition is valid, false otherwise (in this case the state is not changed)
     */
    bool setState( const State s ) {
      // TODO: take care of state transitions!
      // is the ownlock-lock locked right now?
      state = s;
      return true;
    }

    int getID() const { return id; }

    bool isIdle() const { return state == idle; }
    bool isWorking() const { return state == working; }
    bool isAborted() const { return state == aborted; }
    bool isSleeping() const { return state == sleeping; }
    bool isFinished() const { return state == finished; }
    bool isInterrupted() const { return state == interrupted; }
    bool isInterruptForced() const { return state == interruptedForce; }
    bool isWaiting() const { return state == waiting; }
    bool isDoReceive() const { return state == doReceiveFromMaster; }
    bool isFinishedReceiving() const { return state == finishedReceiving; }

    /** update the solver at the current state
     * @param s pointer to the solver that just entered the update method TODO: necessary? Should be equal to ptr in this object!
     * @return true, if something has been done (e.g. a clause has been added), false otherwise
     * note: this method should be called by the solver only if it will be doing a decision next
     */
    bool update( Minisat::Solver* s ){
      // implement update code here!
      return true;
    }

    /** wake up master after some notification
     */
    void awakeMaster() {
      data->getMasterLock().awake(1000);
    }

    /** return a handle to the solver of this communicator
     */
    Minisat::Solver* getSolver() { return solver; }

    /** adds a clause to the next position of the pool
     * @param clause vector that stores the clause to be added
     */
    void addClause( const Minisat::vec<Minisat::Lit>& clause )
    {
      data->getBuffer().addClause(((stamp << 8) | id), clause); // piggy-back the stamp
    }

    /** copy all clauses into the clauses vector that have been received since the last call to this method
     * note: only an approximation, can happen that ringbuffer overflows!
     * note: should be called by the worker solver only!
     */
    void receiveClauses ( Minisat::ClauseAllocator& ca, std::vector< Minisat::CRef >& clauses )
    {
      //unsigned int oldLastSeen = lastSeenIndex;
      lastSeenIndex = data->getBuffer().receiveClauses(((stamp << 8) | id), lastSeenIndex, ca, clauses, nrRejectRcvStamp); // piggy-back the stamp
    }

    void initProtect( const Minisat::vec<Minisat::Lit>& assumptions, const int vars ) {
      protect.clear();
      protect.growTo(vars,0);
      for( int i = 0 ; i < assumptions.size(); ++ i )
	protect[ Minisat::var(assumptions[i]) ] = 1;
    }
    
    // literal is only protected, if this option is enabled
    bool isProtected ( const Minisat::Lit& l ) const {
      return protectAssumptions && protect[ Minisat::var( l ) ]; 
    }

    bool variableProtection() const { return protectAssumptions; }

    // time-stamping; to control the direction ...
    void set_stamp(int st) { stamp = st; }
    int get_stamp(void) { return stamp; }
    
  public:       // this probably is not a good idea, but ah well ...

    bool protectAssumptions;      // should assumption variables not be considered for calculating send-limits?
    float sendSize;               // Minimum Lbd of clauses to send  (also start value)
    float sendLbd;                // Minimum size of clauses to send (also start value)
    float sendMaxSize;            // Maximum size of clauses to send
    float sendMaxLbd;             // Maximum Lbd of clauses to send
    float sizeChange;             // How fast should size send limit be adopted?
    float lbdChange;              // How fast should lbd send limit be adopted?
    float sendRatio;              // How big should the ratio of send clauses be?
    bool  doBumpClauseActivity;   // Should the activity of a received clause be increased from 0 to current activity
    
    unsigned nrSendCls;           // how many clauses have been send via this communicator
    unsigned nrRejectSendSizeCls; // how many clauses have been rejected to be send because of size
    unsigned nrRejectSendLbdCls;  // how many clauses have been rejected to be send because of lbd
    unsigned nrReceivedCls;       // how many clauses have been received (there is no filter yet!!)
    unsigned nrReceive;	          // number of attempts to receive clauses
    unsigned receiveEvery;        // reject clause receptions for N attempts
    unsigned nrRejectRcvStamp;    // number of rejects due to stamp mismatch
    
  };

#endif
